<?php

namespace Noith\OrchidSettings;

use Illuminate\Support\ServiceProvider;
use Orchid\Platform\ItemPermission;
use Orchid\Support\Facades\Dashboard;

class OrchidSettingsProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadRoutesFrom(__DIR__ . '/../routes/platform.php');
        Dashboard::registerPermissions($this->registerPermissions());
    }

    protected function registerPermissions(): ItemPermission
    {
        return ItemPermission::group(__('System'))
            ->addPermission('platform.settings', __('Settings'));
    }
}
