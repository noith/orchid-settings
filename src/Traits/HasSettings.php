<?php

namespace Noith\OrchidSettings\Traits;

use Illuminate\Support\Facades\View;
use Orchid\Platform\Dashboard;
use Orchid\Screen\Actions\Menu;

trait HasSettings
{
    public function registerSettings(): void
    {
        $settings = [];
        if (method_exists($this, 'settings')) {
            $settings = $this->settings();
        }
        Dashboard::macro('settings', fn() => $settings);
        View::composer('platform::dashboard', function () {
            \Orchid\Support\Facades\Dashboard::registerMenuElement(
                Menu::make(__('Settings'))
                    ->icon('bs.wrench-adjustable')
                    ->permission('platform.settings')
                    ->route('platform.settings.list')
            );
        });
    }
}
