<?php

namespace Noith\OrchidSettings\Orchid;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Noith\Settings\Repositories\SettingsRepository;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Dashboard;
use Orchid\Support\Facades\Layout;
use Orchid\Support\Facades\Toast;

class SettingScreen extends Screen
{
    /**
     * Fetch data to be displayed on the screen.
     *
     * @return array
     */
    public function query(SettingsRepository $settings): iterable
    {
        return $settings->all();
    }

    /**
     * The name of the screen displayed in the header.
     *
     * @return string|null
     */
    public function name(): ?string
    {
        return __('Settings');
    }

    /**
     * The screen's action buttons.
     *
     * @return \Orchid\Screen\Action[]
     */
    public function commandBar(): iterable
    {
        return [
            Button::make(__('Save'))->method('save'),
        ];
    }

    /**
     * The screen's layout elements.
     *
     * @return \Orchid\Screen\Layout[]|string[]
     */
    public function layout(): iterable
    {
        $tabs = [];
        foreach (Dashboard::settings() as $tab => $layout) {
            $tabs[$tab] = is_array($layout) ? Layout::rows($layout) : $layout;
        }
        return [Layout::tabs($tabs)];
    }

    public function save(Request $request, SettingsRepository $settings)
    {
        try {
            $settingsRaw = $request->except('_token');
            foreach ($settingsRaw as $key => $setting) {
                if ($settings->$key != $setting) {
                    $settings->set($key, $setting);
                }
            }
            Toast::success(__('Settings successfully saved'));
        } catch (\Throwable $e) {
            Log::error($e->getMessage());
            Toast::error(__('Something went wrong. Please try again later.'));
        }
    }
}
