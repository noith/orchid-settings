<?php

use Illuminate\Support\Facades\Route;
use Noith\OrchidSettings\Orchid\SettingScreen;
use Orchid\Support\Facades\Dashboard;

Route::domain((string)config('platform.domain'))
    ->prefix(Dashboard::prefix('/settings'))
    ->as('platform.settings.')
    ->middleware(config('platform.middleware.private'))
    ->group(function () {
        Route::screen('list', SettingScreen::class)->name('list');
    });
